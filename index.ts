import * as pulumi from "@pulumi/pulumi"
import * as k8s from "@pulumi/kubernetes"

const http = 80
const https = 443
const kubeSystemNamespaceName = 'kube-system'

interface Features {
  ingress: boolean
  certmanager: boolean
  health: boolean
  dashboard: boolean
}

let config = new pulumi.Config()

const stack = pulumi.getStack()
const tier = pulumi.getProject()

const features = config.requireObject<Features>('features')

interface ClusterAdmins {
  paul: boolean
  eric: boolean
}

const clusterAdmins = config.requireObject<Features>('clusterAdmins')

let domain = config.require('domain')
console.log(`Booting up ${domain}...`)

const out: any = {}

for (let [serviceAccountShortName, active] of Object.entries(clusterAdmins)) {
  if (active) {
    const serviceAccount = new k8s.core.v1.ServiceAccount(serviceAccountShortName, {
      metadata: { namespace: kubeSystemNamespaceName, labels: { stack, tier } }
    })

    const privilegedCRB = new k8s.rbac.v1.ClusterRoleBinding(`${serviceAccountShortName}-cluster-admin-binding`, {
      roleRef: {
        apiGroup: 'rbac.authorization.k8s.io',
        kind: 'ClusterRole',
        name: 'cluster-admin'
      },
      subjects: [{
        kind: 'User',
        name: serviceAccount.metadata.name,
        apiGroup: 'rbac.authorization.k8s.io'
      }]
    })

    out[`${serviceAccountShortName}TokenBash`] = pulumi.interpolate`kubectl get secret/${serviceAccount.secrets[0].name} -n kube-system -o go-template='{{.data.token | base64decode}}'`
  }
}
  

/*
* ingress controller
*/

if (features.ingress) {
  const ingressShortName = 'in'
  const ingressPrefix = 'ingress'
  const ingressNamespace = new k8s.core.v1.Namespace(ingressPrefix, {
    metadata: { labels: { name: ingressPrefix, stack, tier } }
  })

  const ingress = new k8s.helm.v3.Chart(ingressShortName, {
    repo: "ingress-nginx",
    chart: "ingress-nginx",
    namespace: ingressNamespace.metadata.name
  }, {
    parent: ingressNamespace
  })

  /*
  * cert manager
  */

  if (features.certmanager) {
    const certmanagerShortName = 'vouch'
    const certmanagerPrefix = 'cert-manager'
    const certmanagerNamespace = new k8s.core.v1.Namespace(certmanagerPrefix, {
      metadata: { labels: { name: certmanagerShortName, stack, tier } }
    }, {
      parent: ingressNamespace
    })

    const certmanager = new k8s.helm.v3.Chart(certmanagerShortName, {
      repo: "jetstack",
      chart: "cert-manager",
      values: {
        installCRDs: true,
        podLabels: { stack, tier }
      },
      namespace: certmanagerNamespace.metadata.name,
    }, {
      parent: certmanagerNamespace
    })

    const clusterIssuerName = 'letsencrypt-staging'

    // const clusterIssuer = new k8s.helm.v3.Chart.ClusterIssuer(
    //   clusterIssuerName, {
    //     metadata: {
    //       labels: { name: certmanagerShortName, stack, tier },
    //       namespace: certmanagerNamespace.metadata.name
    //     },
    //   spec: {
    //     acme: {
    //       server: "https://acme-staging-v02.api.letsencrypt.org/directory",
    //       email: `reply@${domain}`,
    //       privateKeySecretRef: {
    //         name: clusterIssuerName
    //       },
    //       solvers: [{
    //         http01: {
    //           ingress: { class: "nginx" }}
    //       }]
    //     }
    //   }
    // }, {
    //   parent: certmanager
    // })

    /*
    * health check deployment
    */

    if (features.health) {
      const healthPrefix = 'heal'
      const healthNamespace = new k8s.core.v1.Namespace(healthPrefix, {
        metadata: { labels: { name: healthPrefix, stack, tier } }
      }, {
        parent: certmanager,
        dependsOn: [ ingress ]
      })
      const health = healthNamespace.metadata.name

      const healthAppName = 'health'
      const healthAppLabels = { app: healthAppName, stack, tier }

      const healthDeployment = new k8s.apps.v1.Deployment(
        healthAppName, {
        metadata: {
          namespace: health,
          labels: healthAppLabels,
        },
        spec: {
          selector: { matchLabels: healthAppLabels },
          replicas: 1,
          template: {
            metadata: { labels: healthAppLabels },
            spec: { containers: [{ name: healthAppName, image: 'nginx' }] }
          }
        }
      }, {
        parent: healthNamespace
      })

      const healthService = new k8s.core.v1.Service(healthAppName, {
        metadata: {
          namespace: health,
          labels: healthDeployment.spec.template.metadata.labels
        },
        spec: {
          type: "ClusterIP",
          ports: [{ port: http, targetPort: http, protocol: "TCP" }],
          selector: healthAppLabels
        }
      }, {
        parent: healthNamespace
      })

      const healthHost = `${healthAppName}.${domain}`
      out.health = `https://${healthHost}`
      
      const healthIngress = new k8s.networking.v1.Ingress(healthAppName, {
        metadata: {
          namespace: health,
          labels: healthDeployment.spec.template.metadata.labels,
          annotations: {
            'kubernetes.io/ingress.class': 'nginx',
            // Add the following line (staging first for testing, then apply the prod issuer)
            'cert-manager.io/cluster-issuer': 'letsencrypt-staging' // 'letsencrypt-prod'
          }
        },
        spec: {
          tls: [{
            hosts: [healthHost],
            secretName: 'default-tls-secret'
          }],
          rules: [{
            host: healthHost,
            http: {
              paths: [{
                backend: {
                  service: {
                    name: healthService.metadata.name,
                    port: {
                      number: http
                    }
                  }
                },
                path: '/',
                pathType: 'ImplementationSpecific'
              }]
            }
          }
          ]
        }
      }, {
        parent: healthService
      })

    }

    /*
    * dashboard (ingress + role)
    */

    if (features.dashboard) {
      const dashboardShortName = 'dash-basic'
      const kubernetesDashboardServiceName = 'kubernetes-dashboard'

      const dashboardHost = `${dashboardShortName}.${domain}`
      out.dashboard = `https://${dashboardHost}`

      const dashboardServiceAccount = new k8s.core.v1.ServiceAccount('admin-user', {
        metadata: { namespace: kubeSystemNamespaceName, labels: { stack, tier } }
      })

      out.dashboardTokenBash = pulumi.interpolate`kubectl get secret/${dashboardServiceAccount.secrets[0].name} -n kube-system -o go-template='{{.data.token | base64decode}}'`


      const dashboardClusterRoleBinding = new k8s.rbac.v1.ClusterRoleBinding('admin-user', {
        metadata: { labels: { stack, tier } },
        roleRef: {
          apiGroup: 'rbac.authorization.k8s.io',
          kind: 'ClusterRole',
          name: 'cluster-admin'
        },
        subjects: [{
          kind: 'ServiceAccount',
          name: dashboardServiceAccount.metadata.name,
          namespace: dashboardServiceAccount.metadata.namespace
        }]
      })

      const dashboardIngress = new k8s.networking.v1.Ingress(dashboardShortName, {
        metadata: {
          namespace: kubeSystemNamespaceName,
          annotations: {
            'nginx.ingress.kubernetes.io/backend-protocol': 'https',
            'nginx.ingress.kubernetes.io/ssl-passthrough': 'true',
            // Add the following line (staging first for testing, then apply the prod issuer)
            'cert-manager.io/cluster-issuer': 'letsencrypt-staging' // 'letsencrypt-prod'
          },
          labels: { stack, tier }
        },
        spec: {
          tls: [{
            hosts: [dashboardHost],
            secretName: 'default-tls-secret'
          }],
          rules: [{
            host: dashboardHost,
            http: {
              paths: [{
                backend: {
                  service: {
                    name: kubernetesDashboardServiceName,
                    port: { number: https }
                  },
                },
                path: '/',
                pathType: 'ImplementationSpecific'
              }]
            }
          }]
        }
      }, {
        parent: certmanager,
        dependsOn: [ ingress ] 
      })
    }
  }
}

export default {
  features,
  out
}