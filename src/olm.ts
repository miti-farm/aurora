import * as path from 'path';

import * as pulumi from '@pulumi/pulumi';
import * as k8s from '@pulumi/kubernetes';

import { filesDir } from './util';

interface OperatorLifecycleManagerArgs {
  namespace?: pulumi.Input<string>;
  imageRef?: pulumi.Input<string>;
}

export class OperatorLifecycleManager extends pulumi.ComponentResource {
  public readonly chart: k8s.helm.v3.Chart;

  constructor(name: string, args: OperatorLifecycleManagerArgs, opts?: pulumi.ComponentResourceOptions) {
    super("pulumi-extra:k8s:OperatorLifecycleManager", name, {}, opts);

    let {
      namespace = "olm",
      imageRef = "quay.io/operator-framework/olm@sha256:de396b540b82219812061d0d753440d5655250c621c753ed1dc67d6154741607"
    } = args;

    let values = {
      namespace: namespace,
      catalog_namespace: namespace,
      operator_namespace: `${name}-operators`,
      olm: { image: { ref: imageRef } },
      package: { image: { ref: imageRef } },
    };

    let chartPath = path.join(filesDir, "olm/chart");

    // deploy chart
    this.chart = new k8s.helm.v3.Chart(name, {
      path: chartPath, values,
      transformations: [(obj => {
        // remove packageserver, a it needs to have explicit dependency defined
        if (obj.kind == "ClusterServiceVersion" && obj.metadata?.name == "packageserver") {
          obj.apiVersion = "v1";
          obj.kind = "List";
        }
      })]
    }, { parent: this });

    // include only packageserver
    new k8s.helm.v3.Chart(`${name}-packageserver`, {
      path: chartPath, values,
      transformations: [(obj => {
        // remove everything but packageserver, a it needs to have explicit dependency defined
        if (!(obj.kind == "ClusterServiceVersion" && obj.metadata?.name == "packageserver")) {
          obj.apiVersion = "v1";
          obj.kind = "List";
        }
      })]
    }, { parent: this.chart });
  }
}
