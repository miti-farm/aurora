# Aurora

> a natural electrical phenomenon characterized by the appearance of streamers of coloured light in the sky

Base layer of cluster services for an operational cloud.

- cluster admins
- nginx ingress controller
- cert-manager
- health check with an nginx deployment: [health.cuciureanu.com](https://health.cuciureanu.coms)

## Prerequisites

Requires an Internet facing MicroK8s cluster.

## Design

2 clusters on the same LAN.

The `live` cluster is the production cloud, a 3+ nodes high-availability bare metal setup.

The `omega` cluster is a few commits ahead and doubles as a fail-over 1-node VM cluster.

## Develop and Deploy

``` bash
pulumi stack ls # or
pulumi stack init miti-live
# live
pulumi config set kubernetes:context microk8s-cluster
pulumi config set kubernetes:cluster microk8s-cluster
pulumi config set kubernetes:cluster ~/.kube/miti-live-config
# omega
pulumi stack init miti-omega
pulumi config set kubernetes:context microk8s-cluster
pulumi config set kubernetes:cluster microk8s-cluster
pulumi config set kubernetes:cluster ~/.kube/miti-omega-config
```

Pre-requisites for Helm.

``` bash
helm repo add stable https://charts.helm.sh/stable
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo add jetstack https://charts.jetstack.io
```

### Pulumi Stack Config

``` bash
pulumi stack select miti-live
pulumi config set --plaintext domain cuciureanu.com
pulumi config set --plaintext adminEmail reply@cuciureanu.com

# or
pulumi stack select miti-omega
pulumi config set --plaintext domain omega.cuciureanu.com
pulumi config set --plaintext adminEmail reply@cuciureanu.com
```

### NGINX Ingress Controller

``` bash
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo update
```

### Deploy

``` bash
pulumi up -f -y
```

We should now be able to securely access: [health.cuciureanu.com](https://health.cuciureanu.com)

Yey! :fireworks:

## Troubleshooting

Many things could go wrong. :smile:
